"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Contacts",
      [
        {
          firstname: "Osvaldo",
          lastname: "Gonzalez",
          phone: "0000000000",
          email: "osvaldo@email.com",
          createdAt: new Date().toDateString(),
          updatedAt: new Date().toDateString()
        },
        {
          firstname: "Jhon",
          lastname: "Doe",
          phone: "9999999999",
          email: "jhondoe@email.com",
          createdAt: new Date().toDateString(),
          updatedAt: new Date().toDateString()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
