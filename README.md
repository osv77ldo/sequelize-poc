# Sequelize Project
This is a simple CRUD node js project using

- sequelize [](https://sequelize.org/master/)
- sequelize-cli [](https://github.com/sequelize/cli)
- sqlite3 [](https://sqlite.org/)

Sequelize is a promise-based Node.js ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server. It features solid transaction support, relations, eager and lazy loading, read replication and more.

## Run the migrations and seeders
In order to use the sequelize capabilities, you need to use the sequelize-cli, see the official documentation here [](https://github.com/sequelize/cli)

## Run this project (CRUD)
This project receive the argument for command line, so if you want to create, update, delete or Read is mandatory set that param on the execution

  CREATE: "create",
  READ: "read",
  UPDATE: "update",
  DELETE: "delete"